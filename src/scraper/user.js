const { wait } = require("../utils");

async function scrapeUser(page, url) {
    await page.goto(url, { waitUntil: "domcontentloaded" });
    await page.waitForSelector(".sqdOP", { timeout: 3000 });
    await wait(10000);
    let username = await page.evaluate(`
        let usernameEl = document.querySelectorAll(".sqdOP")[0];
        let username = usernameEl ? usernameEl.innerText : null;
        username
    `);
    return username;
}

module.exports = { scrapeUser };
const debug = require("debug")('app:scrape')
const debugErr = require("debug")("err:scrape");

const Queue = require('bull');
const config = require('config');

const { wait } = require('../utils.js')

const redisConfig = config.get('redis');
const scrapeConfig = config.get('scrape');

const tagQueue = new Queue('hashtag', redisConfig.url);

async function scrapeHashtag(page, url) {
    let curPostCount = 0;
    page.on("response", async (resp) => {
        let request = resp.request();
        let reqUrl = await request.url();
        let filter = "query_hash";
        if (reqUrl.includes(filter)) {
            let data = await resp.json();
            if (data.data.hashtag.edge_hashtag_to_media) {
                let posts = data.data.hashtag.edge_hashtag_to_media.edges;
                curPostCount += posts.length;
                debug(`Adding ${posts.length} to the queue for ${url}`);
                posts.forEach((post) => {
                    tagQueue.add({
                        id: post.node.id,
                        metaData: post.node
                    });
                });
            }
        }
    });
    await page.goto(url, { waitUntil: "domcontentloaded" });
    await page.waitForXPath("/html/body/div[1]/section/main/article/div[1]/div/div/div[1]", { timeout: 3000 });
    await page.waitForXPath("/html/body/div[1]/section/main/header/div[2]/div[1]/div[2]/span/span", { timeout: 3000 });
    let elHandle = await page.$x("/html/body/div[1]/section/main/header/div[2]/div[1]/div[2]/span/span");
    let totalPostCount = await page.evaluate(el => el.textContent, elHandle[0]);
    totalPostCount = parseFloat(totalPostCount.replace(/,/g, ''));
    debug("count:", totalPostCount);
    try {
        let previousHeight;
        let retryCount = scrapeConfig.retryCount;
        while (retryCount > 0 && curPostCount < totalPostCount) {
            previousHeight = await page.evaluate(
                "document.documentElement.scrollHeight"
            );
            await page.evaluate(
                "window.scrollTo(0, document.documentElement.scrollHeight)"
            );
            await wait(2000);
            let val = await page.evaluate(
                `document.documentElement.scrollHeight > ${previousHeight}`
            );
            if (!val) {
                await page.evaluate(
                    "window.scrollTo(0, document.documentElement.scrollHeight - 3000)"
                );
                await wait(5000);
                retryCount--;
                debug(`Decrementing retry count: ${retryCount} for ${url}`);
            }
            await page.evaluate('document.body.style = ""');
        }

    } catch (e) {
        debug(`Scrolling limit reached: ${page.url()}`);
    }
}

module.exports = { scrapeHashtag };
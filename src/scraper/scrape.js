
const debug = require("debug")('app:scrape')
const debugErr = require("debug")("err:scrape");

const config = require('config');
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const { Cluster } = require("puppeteer-cluster");

const { scrapeUser } = require("./user");
const { scrapeHashtag } = require("./hashtag");

const scrapeConfig = config.get("scrape");
const clusterConf = config.get('cluster');

let cluster;
puppeteer.use(StealthPlugin());

const clusterOptions = {
    concurrency: Cluster.CONCURRENCY_BROWSER,
    puppeteer,
    ...clusterConf,
};

(async () => {
    cluster = await Cluster.launch(clusterOptions);
    await cluster.task(async ({ page, data: data }) => {
        try {
            switch (data.type) {
                case "user":
                    await scrapeUser(page, data.uri);
                    break;
                case "hashtag":
                    await scrapeHashtag(page, data.uri);
                    break;
            }
        } catch (e) {
            debugErr(e);
        }
    });

    cluster.on("taskerror", (err, data) => {
        debugErr(`${data}: ${err.message}`);
    });
    await cluster.idle();
})();

async function enqueueUser(shortcode) {
    let baseURI = scrapeConfig.baseURL;
    let uri = `${baseURI}p/${shortcode}`;
    cluster.queue({ type: "user", uri: uri });
}

async function enqueueHashtag(hashtag) {
    let baseURI = scrapeConfig.baseURL;
    let uri = `${baseURI}explore/tags/${hashtag}`;

    debug(`Enqueued ${hashtag}`)
    cluster.queue({ type: "hashtag", uri: uri });
}

module.exports = {
    enqueueUser,
    enqueueHashtag
}
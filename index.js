const csv = require("neat-csv");
const fs = require("fs");
const config = require('config');

const { enqueueHashtag } = require("./src/scraper/scrape");

const scrapeConfig = config.get("scrape");
const debug = require("debug")("app:index");
const debugErr = require("debug")("err:index");

let hashtags;

fs.readFile(scrapeConfig.source, async (err, data) => {
    if (err) {
        debugErr(err);
        return;
    }
    let res = await csv(data);
    hashtags = res.map((obj) => obj.hashtags);
    debug(`Source File: ${scrapeConfig.source}`);
    debug(`Starting to scrape ${hashtags.length} hashtags`);
    (async () => {
        hashtags.forEach(async (hashtag) => {
            await enqueueHashtag(hashtag);
        });
    })();
});